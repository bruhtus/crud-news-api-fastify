const build = require('../apps/app');

describe('CRUD route exist', () => {
  let app = build();

  test('can create item', async () => {
    const item = {
      title: 'jakarta banjir',
      content: 'jakarta tenggelam'
    };

    const res = await app.inject({
      method: 'POST',
      url: 'api/news',
      payload: item
    });

    expect(res.statusCode).toBe(201);
    expect(res.json()).toEqual(item);
  });
});

const Fastify = require('fastify');

function app() {
  const fastify = Fastify();

  fastify.post('/api/news', async (req, res) => {
    return res.status(201).send(req.body);
  });

  return fastify;
}

module.exports = app;
